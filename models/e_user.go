package models

import (
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"time"
	"e_answer/log"
)


type Euser struct {
	Id       		bson.ObjectId    	`json:"id" bson:"_id"`
	Account  		string 				`json:"account" bson:"account"`
	Password 		string             	`json:"password" bson:"password"`
	IMei			string 				`json:"imei" bson:"imei"`
	Email 			string 				`json:"email" bson:"email"`
	Mobile      	string 				`json:"mobile" bson:"mobile"`//手机号
	AliAccount  	string 				`json:"ali_account" bson:"ali_account"`//支付宝账号
	WeiChatAccount 	string 				`json:"weichat_account" bson:"weichat_account"`//微信账号
	Status 			int   				`json:"status" bson:"status"` //状态，10正常；20账号被封
	Attentions		[]int64             `json:"attentions" bson:"attentions"` //关注
	Collections		[]int64 			`json:"collections" bson:"collections"` //收藏
	Ctime     		int64 	 			`json:"-" bson:"ctime"`//创建时间
	Mtime     		int64 				`json:"-" bson:"mtime"`//修改时间
}

type EuserDao struct {
	gsession    *mgo.Session    // global session
	gcollection *mgo.Collection // global colection
}

func NewEuserDao(session *mgo.Session, dbName string) *EuserDao {
	return &EuserDao{gsession: session, gcollection: session.DB(dbName).C("e_user")}
}

func (this *EuserDao) copySession() (*mgo.Session, *mgo.Collection) {
	session := this.gsession.Copy()
	collection := this.gcollection.With(session)
	return session, collection
}

//账号注册
func (this *EuserDao)AccountInsert(euser *Euser)(err error){
	session, collection := this.copySession()
	defer session.Close()

	euser.Id=bson.NewObjectId()
	euser.Ctime=time.Now().Unix()
	euser.Mtime=euser.Ctime

	err=collection.Insert(euser)
	return
}

//查找
func (this *EuserDao)FindById(id string)(euser *Euser,err error){
	session, collection := this.copySession()
	defer session.Close()
	idBson:=bson.ObjectIdHex(id)

	err=collection.FindId(idBson).One(euser)
	return
}

//更新
func (this *EuserDao)Update(euser *Euser)(err error) {
	session, collection := this.copySession()
	defer session.Close()

	euser.Mtime=time.Now().Unix()
	err=collection.UpdateId(euser.Id,euser)
	return
}

//游客登录、注册
func (this *EuserDao)ImeiQusert(imei string)error{
	session, collection := this.copySession()
	defer session.Close()

	//根据imei查找
	count,err:=collection.Find(bson.M{"imei":imei}).Count()
	if err!=nil{
		log.LOG.Error("get euser by imei error:[%v],imei:[%s]",err,imei)
		return err
	}

	//已注册，返回
	if count>0{
		log.LOG.Debug("imei has registered,count of imei is [%d]",count)
		return nil
	}

	//注册新游客
	euser:=new(Euser)
	euser.Id=bson.NewObjectId()
	euser.Ctime=time.Now().Unix()
	euser.Mtime=euser.Ctime

	err=collection.Insert(euser)
	if err!=nil{
		log.LOG.Error("imei register error when insert euser,err:[%v],euser:[%+v]",err,*euser)
	}
	return err
}