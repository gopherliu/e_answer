package models

import (
	"e_answer/config"
	sf "github.com/yzw/snowflake"
	"e_answer/log"
)


var (
	ID_GENERATOR *sf.Snowflake
)

func Init(conf *config.Config) (err error) {

	//ID生成器
	ID_GENERATOR, err = sf.NewSnowflake(0, 0)
	if err != nil {
		log.LOG.Error("ID_GENERATOR error:[%v]",err)
		panic(err)
		return
	}
	return nil
}
