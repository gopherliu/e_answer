package app

import (
	"github.com/garyburd/redigo/redis"
	"gopkg.in/mgo.v2"
	"e_answer/config"
	"time"
"e_answer/models"
)


var (
	session *mgo.Session
	REDISPOOL *redis.Pool //redis 数据库连接池
	redisPre string //redis key 前缀
//	redisEmailVerifyPath string //redis 邮件前缀
	redisKeyDuration int
//	repassText string
//	stmp_user string
//	stmp_pass string
//	stmp_port string
//	stmp_addr string
//	restaurantDao * models.RestaurantDao
	euserDao  * models.EuserDao
	ErrNotFound error
)
func Init(conf *config.Config) {
	var err error

	//mongoDB配置
	mongoDBDialInfo := &mgo.DialInfo{
		Addrs:    []string{conf.DbMongoAddr},
		Timeout:  60 * time.Second,
		Database: conf.DBMongoName,
	}
	session, err = mgo.DialWithInfo(mongoDBDialInfo)
	if err != nil {
		panic(err)
	}
	session.SetMode(mgo.Monotonic, true)

	//redis配置
	REDISPOOL = &redis.Pool{
		MaxIdle:     1,
		MaxActive:   20,
		IdleTimeout: 180 * time.Second,
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial("tcp", conf.RedisAddr)
			if err != nil {
				return nil, err
			}
			if conf.IsRedisAuth {
				if _, err := c.Do("AUTH", conf.RedisAuth); err != nil {
					c.Close()
					return nil, err
				}
			}
			return c, nil
		},
	}

	redisPre=conf.RedisPrefix
//	redisEmailVerifyPath=conf.RedisEmailVerifyPath
	redisKeyDuration=conf.RedisKeyDuration
//	repassText=conf.RePassText
//
//	//邮箱配置
//	stmp_addr=conf.SmtpAddr
//	stmp_user=conf.SmtpUser
//	stmp_pass=conf.SmtpPass
//	stmp_port=conf.SmtpPort
//
//	//restraurantDao 初始化
//	restaurantDao=models.NewRestaurantDao(session,conf.DBMongoName)
	euserDao=models.NewEuserDao(session,conf.DBMongoName)
	ErrNotFound=mgo.ErrNotFound

	return
}

func Clean() {
	session.Close()
}
