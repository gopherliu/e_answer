package app

import (
	"fmt"
	"time"
	"crypto/md5"
	"encoding/hex"
	"errors"
	"github.com/garyburd/redigo/redis"
	"e_answer/log"
)

func  Md5(str string) string {
	newStr := md5.New()
	newStr.Write([]byte(str))
	return hex.EncodeToString(newStr.Sum(nil))
}

func CreateToken(id string) (string, error) {
	conn := REDISPOOL.Get()
	defer conn.Close()

	redisKey := fmt.Sprintf("%s:%s:%s", redisPre, "user_token", id)

	if ok, _ := redis.Bool(conn.Do("EXISTS", redisKey)); ok {
		return redis.String(conn.Do("GET", redisKey))
	}

	uidMd := Md5(fmt.Sprintf("%d:%d", id, time.Now().Unix()))
	if _, err := conn.Do("SETEX", redisKey, redisKeyDuration, uidMd); err != nil {
		return "", err
	}

	redisKey2 := fmt.Sprintf("%s:%s:%s", redisPre, "token_user", uidMd)
	if _, err := conn.Do("SETEX", redisKey2, redisKeyDuration, id); err != nil {
		return "", err
	}

	return uidMd, nil
}

func CheckToken(token string)(string,error){
	conn := REDISPOOL.Get()
	defer conn.Close()

	redisKey2 := fmt.Sprintf("%s:%s:%s", redisPre, "token_user", token)
	if ok, err:= redis.Bool(conn.Do("EXISTS", redisKey2)); !ok {
		log.LOG.Error("token_user key:[%s],err:[%v]",redisKey2,err)
		return "", errors.New("token不存在或已过期")
	}

	id, err := redis.String(conn.Do("GET", redisKey2))
	if err != nil {
		log.LOG.Error("token_user key:[%s],err:[%v]",redisKey2,err)
		return "", err
	}

	redisKey := fmt.Sprintf("%s:%s:%s", redisPre, "user_token", id)
	user_token, err := redis.String(conn.Do("GET", redisKey))
	if err != nil {
		log.LOG.Error("user_token key:[%s],err:[%v]",redisKey,err)
		return "", err
	}

	if user_token != token {
		return "", errors.New("token不对")
	}
	return id, nil
}

func DelToken(token string)(err error){
	conn := REDISPOOL.Get()
	defer conn.Close()

	redisKey2 := fmt.Sprintf("%s:%s:%s", redisPre, "token_user", token)
	id, err := redis.String(conn.Do("GET", redisKey2))
	if err != nil {
		log.LOG.Error("token_user key:[%s],err:[%v]",redisKey2,err)
		return err
	}

	redisKey := fmt.Sprintf("%s:%s:%s", redisPre, "user_token", id)
	_, err = redis.Int(conn.Do("DEL", redisKey))
	if err != nil {
		log.LOG.Error("del key:[%s],err:[%v]",redisKey,err)
		return  err
	}

	_, err = redis.Int(conn.Do("DEL", redisKey2))
	if err != nil {
		log.LOG.Error("del key:[%s],err:[%v]",redisKey2,err)
		return  err
	}

	return
}