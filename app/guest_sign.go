package app

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"e_answer/log"
)

//游客登录
type GuestSignArgs struct{
	Imei 		string 		`json:"imei"`
}
type GuestSignResp struct {
	ErrCode 	int 		`json:"err_code"`
	ErrMessage 	string 		`json:"err_message"`
	Token       string   	`json:"token"`
}

func GuestSign(c *gin.Context){
	imei:=c.Param("imei")
	resp:=GuestSignResp{}

	log.LOG.Debug("imei commit by client is:[%s]",imei)

	//imei验证
	if imei==""{
		log.LOG.Error("imei is nil")
		resp.ErrCode=2001
		resp.ErrMessage="imei不能为空"
		c.JSON(http.StatusOK,resp)
		return
	}

	//IMEI登录/注册
	err:=euserDao.ImeiQusert(imei)
	if err!=nil{
		log.LOG.Error("error when guest login,err:[%v],imei:[%s]",err,imei)
		resp.ErrCode=2000
		resp.ErrMessage="服务器内部错误"
		c.JSON(http.StatusOK,resp)
		return
	}


}