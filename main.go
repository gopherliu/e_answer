package main

import (
	"github.com/gin-gonic/gin"
	"flag"
	"github.com/astaxie/beego/logs"
	"e_answer/config"
	"e_answer/log"
	"io/ioutil"
	"encoding/json"
	"e_answer/app"
	"e_answer/admin"
	"e_answer/models"
)

var (
	confFile string
	mode     string
	conf *config.Config
)

func init(){

	//初始化配置文件以及运行模式
	flag.StringVar(&confFile, "c", "", "conf filename")
	flag.StringVar(&mode, "m", "", "app or admin")
	flag.Parse()
	if mode != "app" && mode != "admin" {
		panic("必须指定mode为app或admin")
	}
	var err error
	var jsonBlob []byte
	if jsonBlob, err = ioutil.ReadFile("config/conf.json"); err != nil {
		panic(err)
	}
	conf = new(config.Config)
	if err = json.Unmarshal(jsonBlob, conf); err != nil {
		panic(err)
	}

	//日志文件配置
	log.LOG=logs.NewLogger(10000)
	log.LOG.SetLogger("file", `{"filename":"log/run.log"}`)
	log.LOG.EnableFuncCallDepth(true)

}

func main()  {
	err:=models.Init(conf)
	if err!=nil{
		log.LOG.Error("models init error:[%v]",err)
		panic(err)
	}
	r := gin.Default()

	//管理后台模式
	if mode=="admin"{

		//admin初始化
		admin.Init(conf)
	}

	//app后端模式
	if mode=="api"{
		app.Init(conf)
		defer app.Clean()

		//访问API
		r.POST("/guest_sign",app.GuestSign)
	}
}