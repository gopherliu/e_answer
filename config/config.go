package config

type Config struct {
	Debug      				bool   	`json:"Debug"`
	SqlConn    				string 	`json:"SqlConn"`
	SqlShowLog 				bool   	`json:"SqlShowLog"`

	BindAdmin 				string 	`json:"BindAdmin"`
	BindApi   				string 	`json:"BindApp"`

	RedisPrefix  			string 	`json:"RedisPrefix"`
	IsRedisAuth   			bool  	`json:"IsRedisAuth"`
	RedisAddr    			string 	`json:"RedisAddr"`
	RedisAuth    			string 	`json:"RedisAuth"`
	RedisMaxConn 			int    	`json:"RedisMaxConn"`
	RedisEmailVerifyPath 	string 	`json:"RedisEmailVerifyPath"`
	RedisVerifyCodeExpire 	int 	`json:"RedisVerifyCodeExpire"`
	RedisKeyDuration 		int 	`json:"RedisKeyDuration"`
	DbMongoAddr             string 	`json:"DbMongoAddr"`
	DBMongoName             string 	`json:"DBMongoName"`
	DBMongoUsername         string 	`json:"DBMongoUsername"`
	DBMongoPassword         string 	`json:"DBMongoPassword"`
}
